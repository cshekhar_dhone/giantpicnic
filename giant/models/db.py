# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

if 0: # Stuff for Eclipse
    global request
    global session
    global response

    from gluon.globals import Request, Session, Response
    from gluon.sqlhtml import CAT, A, URL
    from gluon.tools import Crud
    from gluon.dal import DAL, Field
    from gluon.validators import CRYPT, IS_EMAIL, IS_MATCH
    from gluon.translator import translator as T
    
    req = Request()
    req = request
    ses = Session()
    ses = session
    resp = Response()
    resp = response
    
    crud = Crud()

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL('sqlite://storage.sqlite')
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager
auth = Auth(db, hmac_key=Auth.get_or_create_key())
crud, service, plugins = Crud(db), Service(), PluginManager()

auth.settings.table_user = db.define_table(
    'auth_user',
    Field('name', length=128, label=T('Name'), default='', readable=False, writable=False),
    Field('email', length=128, label=T('Email'), default='', requires=IS_EMAIL(), unique=True),
    Field('password', 'password', readable=False, label=T('Password'), requires=CRYPT()),
    Field('interests', 'list:string'),
    Field('registration_key', length=128, default='', readable=False, writable=False)
)

auth.define_tables(username=False, signature=False)
auth.settings.register_verify_password = False
auth.settings.login_after_registration = True

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

'''
from fbappauth import CLIENT_ID, CLIENT_SECRET
from facebook import GraphAPI, GraphAPIError
from gluon.contrib.login_methods.oauth20_account import OAuthAccount

class FacebookAccount(OAuthAccount):
    """OAuth impl for FaceBook"""
    AUTH_URL = "https://graph.facebook.com/oauth/authorize"
    TOKEN_URL = "https://graph.facebook.com/oauth/access_token"
    def __init__(self, g):
        OAuthAccount.__init__(self, g, CLIENT_ID, CLIENT_SECRET, self.AUTH_URL,
                              self.TOKEN_URL, scope='user_photos,friends_photos')
        self.graph = None
    def get_user(self):
        # Returns the user using the Graph API.
        if not self.accessToken():
            return None
        if not self.graph:
            self.graph = GraphAPI((self.accessToken()))
            user = None
        try:
            user = self.graph.get_object("me")
    
        except GraphAPIError, e:
            self.session.token = None
            self.graph = None
        if user:
            return dict(first_name=user['first_name'], last_name=user['last_name'],
                        username=user['id'], registration_id=user['id'])
'''
#from gluon.contrib.login_methods.linkedin_account import LinkedInAccount

#auth.settings.login_form=LinkedInAccount(request,'hidden','hidden',URL('bar'))

crud.settings.formstyle = 'bootstrap'

navbar2 = CAT('Welcome ', auth.user.name, ' [ ', A(T('Logout'), _href=URL('/user/logout')),
              ' | ', A(T('Profile'), _href=URL('/profile')), ' | ',
              A(T('Password'), _href=URL('/user/change_password')),
              ' ] ') if auth.user else CAT('[ ',  A(T('Login'), _href=URL('/login')),
                                           ' | ', A(T('Register'), _href=URL('/register')), ' ]')

#auth.settings.actions_disabled = ['register', 'change_password', 'request_reset_password', 'profile']
#auth.settings.login_form = FacebookAccount(globals())
#auth.navbar(referrer_actions='profile')
#auth.settings.login_next = '/profile/'
#print auth.settings.login_next

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key

#print request

#from gluon.contrib.login_methods.rpx_account import use_janrain, RPXAccount
#use_janrain(auth, filename='private/janrain.key')


#auth.settings.login_form = RPXAccount(
#                                      request,
#                                      api_key='hidden', domain='hidden',
#                                      url='https://'+request.wsgi.environ['HTTP_HOST']+(request.vars._next or '')
#                                      )

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)
