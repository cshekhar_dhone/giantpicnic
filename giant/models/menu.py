# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

response.title = ' '.join(word.capitalize() for word in request.application.split('_'))
response.subtitle = T('Lets go steal some picnic baskets!')

## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'Samuel Marks <samuelmarks@gmail.com>'
response.meta.title = 'GiantPicnic'
response.meta.description = 'an online social-network for organising offline meetups'
response.meta.keywords = 'meetups, events, groups, social-network'
response.meta.generator = 'Web2py Web Framework'
response.meta.copyright = 'Copyright 2012'

## your http://google.com/analytics id
response.google_analytics_id = None

#########################################################################
## this is the main application menu add/remove items as required
#########################################################################

response.menu = [
    [CAT(I(_class='icon-screenshot icon-white'), T(' Groups')), False, URL('', 'groups', ' ')],
    [CAT(I(_class='icon-time icon-white'), T(' Events')), False, URL('', 'events', ' ')],
    (T('Competitors?'), False, '', [(T('Get source'), False, 'http://bitbucket.org/SamuelMarks/giantpicnic')])
]
