# -*- coding: utf-8 -*-

if 0: # Stuff for Eclipse
    from gluon.dal import DAL
    from gluon.tools import Auth, Service, Crud
    from gluon.sqlhtml import SQLTABLE, SQLFORM
    from gluon.languages import translator as T

    db = DAL()
    auth = Auth()
    service = Service()
    crud = Crud()
    
    global db
    global request
    global session
    global response
    from gluon.globals import Request, Session, Response
    
    req = Request()
    req = request
    ses = Session()
    ses = session
    resp = Response()
    resp = response

def index():
    if len(request.args): page = int(request.args[0])
    else: page = 0
    items_per_page = 2
    limitby = (page * items_per_page, (page + 1) * items_per_page + 1)
    events_list = db(db.event.group_id == db.picnics.id).select(orderby=db.event.event_datetime)   
    create = crud.create(db.event)
    
    search_form = SQLFORM.factory(Field('Search', requires=IS_MATCH('^[\w| ]*$')))
    parent = search_form.element('input[type=submit]').parent
    parent.components = [DIV(TAG.BUTTON(TAG.I(_class='icon-search'), _class='btn'))]

    search_result = (None, False)
    if search_form.process().accepted:
        session.search_term = search_form.vars.Search
        query = (db.event.event_name.contains(session.search_term) | 
                  db.event.event_desc.contains(session.search_term))
        search_result = (db(query).select(db.event.event_name, db.event.event_desc,
                                          db.event.event_datetime),
                         True)
    
    return dict(events_list=events_list, page=page, items_per_page=items_per_page,
                create=create, search_form=search_form, search_result=search_result)

@auth.requires_login()
def create_event():
    return crud.create(db.event)
